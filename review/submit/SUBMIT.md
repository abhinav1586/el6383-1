Project Review
Please fill in your name and net ID in the table below.
Lab Assignment	Project Review
Name: Abhinav Verma	
Net ID: N19964723	
Assigned project (#)24	
Review due: 05/04/2015	Monday, 4 May 11:59PM
Please write detailed comments answering the questions below. Do not just answer "yes" or "no" - comment on how well the authors address each of these aspects of experimentation, and offer suggestions for improvement.
Overview
1) Briefly summarize the experiment in this project.

This experiment is based on measuring speed of data transmission protocols UDT and TCP with different bandwidth and under different network 
conditions and plotting it in MATLAB.
The conditions are for 100M,500M and 1000M
1) 0 packet loss and 0 delay.
2) 0.01 packet loss and 0ms delay
3) 0 packet loss and 50ms delay.
2) Does the project generally follow the guidelines and parameters we have learned in class?
Yes, the project follows the guidelines and parameters we have learned in class.

Experiment design
1) What is the goal of this experiment? Is it a focused, specific goal? Is it useful and likely to have interesting results?
The goal of the experiment is to measure the speed of  data transmission protocols UDT and TCP in a simple network under varying conditions. The goal is not specific as it does not state what are we acheiving by measuring the speed of both the protocols. The more appropriate goal should have been to compare the performance of TCP and UDP.
2) Are the metric(s) and parameters chosen appropriate for this experiment goal? Does the experiment design clearly support the experiment goal?
Yes the parameters (bandwidth, packet loss and delay) and metric (throughput) are well appropriate for this experiment goal .Yes, the experiment design does support  the goal as the goal of the experiment is to measure the speed of the data transmission protocols TCP and UDT and the metric being measured is throughput.
3) Is the experiment well designed to obtain maximum information with the minimum number of trials?
Yes the experiment is well designed to obtain the maximum information from minimum number of trail as just by varying the bandwidth, delay and packet loss trials can be performed.
4) Are the metrics selected for study the right metrics? Are they clear, unambiguous, and likely to lead to correct conclusions? Are there other metrics that might have been better suited for this experiment?
Yes the metrics selected for the study of this experiment are right as the goal is to measure speed and the metric is throughput. In my opinion I think the throughtput is a good metric suited to the experiment
5) Are the parameters of the experiment meaningful? Are the ranges over which parameters vary meaningful and representative?
Yes the parameters (bandwidth, packet loss and delay) in this experiment are meaningful as they can provide enough conditions to measure the performance. No, I think the ranges chosen for bandwidth are very small. We can get better result if we use 10M, 100M,500M and 1000M of bandwidth will provide better range to compare, instead of 100M, 500M and 1000M.
6) Have the authors sufficiently addressed the possibility of interactions between parameters?
No, author doesn’t provide any interaction between parameters. There is no explanation as to how  change in parameters will affect the performance of UDT and TCP.
7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate and realistic?
No comparison is made as only speed is measured for TCP and UDP.
Communicating results
1) Do the authors report the quantitative results of their experiment?
Yes the authors have reported the quantitative results. They have plotted a line plot using Matlab between the bandwidth and the throughput of of UTD and FTP.But the results don’t support the goal of the experiment.
2) Is there information given about the variation and/or distribution of experimental results?
There is no information provided by the authors apart from the MATLAB script.
3) Do the authors practice data integrity - telling the truth about their data, avoiding ratio games and other practices to artificially make their results seem better?
No data files has been provided by the authors,so I can’t really conclude whether author have practiced data integrity. There is no data to compare.
4) Is the data presented in a clear and effective way? If the data is presented in graphical form, is the type of graph selected appropriate for the "story" that the data is telling?
Data is presented in a graphical form  which is very clear and it clearly shows the plot between throughput and bandwidth.The graph selected tells us the variation of throughput for file transfer for different Bandwidth under different delays and packet losses.
5) Are the conclusions drawn by the authors sufficiently supported by the experiment results?
There is no conclusion provided by the authors in the project report. After performing the experiment I came to a conclusion that UDT is more effective way to transfer files of large sizes than TCP as its performance is better than the TCP under different parameter setup.
Reproducible research
1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results, Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear and easy to understand?
Yes the author provided the instructions but they are incomplete and diaorganized . They only provided the instructions for setting up the experiment. No data files were available to reproduce results from raw data .Also, no login information was provided to produce experimental data from existing experimental setup. 
2) Were you able to successfully produce experiment results?
We were able to design the topology using the Rspec file and to produce data following the commands given in the instruction. The post scripting was done in MATLAB and using the MATLAB script provided we were able to produce the plot same as in the project report. 
3) How long did it take you to run this experiment, from start to finish?
It took less than 2 hrs to run this experiment from start to finish.
4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe in detail. How long did these extra steps or changes take to figure out?
I didn’t need to make any major changes or additional steps in the experiment except the IP Firewall which is used to change delay, packet loss and bandwidth in the delay pipe. There was  a minor mistake in the following commands
Sudo ipfw pipe 60111 config bw 100M delay 0 plr 0
Sudo ipfw pipe 60111 config bw 100M delay 0 plr 0
It should be –
Sudo ipfw pipe 60111 config bw 100M delay 0 plr 0
Sudo ipfw pipe 60121 config bw 100M delay 0 plr 0.
5) In the lecture on reproducible experiments, we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?
The experiment falls on the 4 degree of reproducibility as  the experiment is simple enough and commands are easy to understand. Only the Matlab code is a little difficult to comprehend as it has no comments describing each step followed in the code. 
Other comments to authors
Please write any other comments that you think might help the authors of this project improve their experiment.

The report is very brief.  Authors needed to make the report more descriptive so as to make the experiment more reproducible.Matlab code is difficult to understand as it didn’t include proper comments.
